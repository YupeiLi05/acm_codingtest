import React from 'react';
import './welcome.css';

interface Props{
    beginGame() : void
}

const Welcome: React.FC<Props> = (props: Props) => {
    const { beginGame } = props;
    return (
        <div className='container'>
            <h1>Welcome Kanji Learning</h1>
            <button className='start-button' onClick={ beginGame }>Start</button>
        </div>
    );
}

export default Welcome;