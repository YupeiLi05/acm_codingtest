import React from 'react';
import './score.css';

interface Props{
    statistic:{
        time: number,
        score: number,
    },
    resetGame() : void
}

const Score: React.FC<Props> = (props: Props) => {
    const { statistic, resetGame } = props;
    return (
        <div className='learning-box'>
            <h1>Welcome Kanji Characters Learning</h1>
            <h3>Score: {statistic.score} / 10</h3>
            <h3>Average Time: {statistic.time / 10 / 10} sec/kanji</h3>
            <button className='replay-button' onClick={ resetGame }>Replay</button>
        </div>
    );
}

export default Score;