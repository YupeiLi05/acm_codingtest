import React, { useState, useEffect, useMemo } from 'react';
import Question from '../utlis/question';
import formatTime from '../utlis/formatTime';
import './learning.css';

interface Props{
    finishGame(time:number, score:number) : void
}

const defaultQues = {
    question: '',
    answerSet: [''],
    answer: ''
}

const Learning: React.FC<Props> = (props: Props) => {
    const [time, setTime] = useState(0);
    const [score, setScore] = useState(0);
    const [timerID, setTimerID] = useState(0);
    const [progress, setProgress] = useState(0);
    const [questionDisplay, setQuestionDisplay] = useState({ ...defaultQues })
    const questionClass = useMemo(()=> new Question(), []);
    const {question, answerSet, answer} = questionDisplay;
    const { finishGame } = props;

    const addScore = () => setScore(preScore => preScore + 1);
    const next = () => setProgress(preProg => preProg + 1);
    const handleAnswer = (v: string) => () => {
        if(v === answer){
            addScore();
            next();
            return;
        }
        next();
    }

    useEffect(() => {
        let id = 0;
        id = window.setTimeout(function timer(){
            setTime(preTime => preTime + 1);
            const id = window.setTimeout(timer, 100);
            setTimerID(id);
        }, 100);
        setTimerID(id);
        return () => {
            window.clearTimeout(timerID);
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(()=>{
        if(progress >= 10 || time >= 250){
            window.clearTimeout(timerID);
            finishGame(time, score);
        }
    }, [progress, time, score, timerID, finishGame]);

    useEffect(()=>{
        const [question, answerSet, answer] = questionClass.getNext();
        question && setQuestionDisplay({
            question: question!, 
            answerSet: answerSet!, 
            answer: answer!,
        })
    }, [progress, questionClass, questionClass.ans.length])

    return (
        <div className='learning-box'>
            <h1 className='header-box'>Welcome Kanji Characters Learning</h1>
            <div className='second-container'>
                <div className='box-container'>
                    <span className='progress-box'>{progress} / 10</span>
                    <div className='question-box'>
                        <span className='question-word'>
                            {question}
                        </span>
                    </div>
                </div>
                <span className='timer-box'>{formatTime(time)}</span>
            </div>
            <div className='answer-box'>
                <div className='answer-container'>
                    {answerSet.map(v=>(
                        <span key={v} className='answer-word' onClick={ handleAnswer(v) }>{v}</span>
                    ))}
                </div>
                <button className='skip-button' onClick={ next }>skip</button>
            </div>
        </div>
    );
}

export default Learning;

//<button onClick={ answer }>add</button>
//<button onClick={ finish }>finish</button>