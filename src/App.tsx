import React, { useState } from 'react';
import WelcomePage from './Components/Welcome';
import LearningPage from './Components/Learning';
import ScorePage from './Components/Score';
import './App.css';

enum STATE {
  Ready,
  Begin,
  End
}

function App() {
  const [gameState, setGameState] = useState(STATE.Ready);
  const [statistic, setStatistic] = useState({time: 0, score: 0});

  const beginGame = ():void => {
    setGameState(STATE.Begin);
  }
  const finishGame = (time:number, score:number): void => {
    setStatistic({time, score});
    setGameState(STATE.End);
  }
  const resetGame = ():void => {
    setStatistic({time:0, score:0});
    setGameState(STATE.Ready);
  }

  const isBegin = ():boolean => gameState === STATE.Begin;
  const isReady = ():boolean => gameState === STATE.Ready;
  const isEnd = ():boolean => gameState === STATE.End;

  return (
    <div className="App">
      {isReady() && <WelcomePage beginGame={beginGame} />}
      {isBegin() && <LearningPage finishGame={finishGame} />}
      {isEnd() && <ScorePage resetGame={resetGame} statistic={statistic} />}
    </div>
  );
}

export default App;
