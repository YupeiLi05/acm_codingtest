function formatTime (time: number){
    return `${parseInt(''+time / 10)}`.padStart(2, '0') + ':' + `${time % 10}`.padEnd(2, '0');
}

export default formatTime