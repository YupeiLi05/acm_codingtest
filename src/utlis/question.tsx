interface QuestionNode {
    word: string,
    appear: boolean,
}

const getRandom = () : boolean => {
    return Math.round(Math.random() * 10) % 2 ===0 ;
}

class Question {
    ques: Array<QuestionNode> = [];
    ans: Array<string> = [];

    constructor(){
        this.init();
    }

    init(){
        fetch('./words.json')
        .then(res => res.json())
        .then(v=>{
            console.log(v);
            v.data.forEach( (element: Array<string>, index: number) => {
                this.ques[index] = {word: element[0], appear: false};
                this.ans[index] = element[1];
            });
        })
        .catch(e=>{
            console.log(e);
        });
    }

    getAnswer(target: number): Array<string> {
        const len = this.ans.length;
        const re: Array<string> = [];
        let correctIn = false;
        for(let i = 0; i < len; i++){
            const reLen = re.length;
            if(reLen >= 3){return re;}

            const select = getRandom();
            if(i === target){
                correctIn = true;
                re.push(this.ans[i]);
                continue;
            }
            if(reLen < 2){
                select && re.push(this.ans[i]);
                continue;
            }
            else{
                if(correctIn){
                    select && re.push(this.ans[i]);
                    continue;
                }
                re.push(this.ans[target]);
                continue;
            }
        }
        if(re.length < 3){
            return this.getAnswer(target);
        }
        return [];
    }

    getNext(): [string?, Array<string>?, string?]{
        if(this.ques.length === 0){return [undefined, undefined, undefined]}
        const len = this.ques.length;
        for(let i = 0; i < len; i++){
            if(this.ques[i].appear){continue;}
            const select = getRandom();
            if(select){
                console.log(i);
                this.ques[i].appear = true;
                return [this.ques[i].word, this.getAnswer(i), this.ans[i]];
            }
        }
        const last = this.ques.findIndex(v=> !v.appear);
        console.log(this.ques);
        return [this.ques[last].word, this.getAnswer(last), this.ans[last]];
    }
}

export default Question;